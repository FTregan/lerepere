# lerepere

## idée de sujets sur lesquels s'entrainer

 - les regexp
 - rust pour les developpeur java (pile, borrow checker, orientation un peu objet - un peu fonctionnelle)
 - construire son robot et le programmer en rust
 - les boucles
 - linux from scratch / c from scratch (suivre les videos d'imilnb)
 - event storming
 - elm
 - apprendre à se vendre aux tarifs suisse / uk / us
 - ergonomie (choisir et regler un fauteuil, orienter son écran, ...)
 - taper sans se fatiguer / sans y penser / plus vite
 - VI (suivre pragmatic VI ?)
 - Attaques par debordement de pile (microcorruption.com ?)
 - spectre / meltdown -> arriver a les exploiter
 - lecture "Initiation pratique à l'emploi des circuits intégrés digitaux"
 - les boucles
 - programmation sur atari 2600 / NES / SNES 
